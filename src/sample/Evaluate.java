package sample;

import java.util.ArrayList;
import java.util.List;

public class Evaluate {
    static double evaluate (String[] postExp){
        Stack workStack = new Stack();
        postExp = clearNull(postExp);
        for (String s: postExp) {
            if (InfixToPostfix.isNumeric(s))
                workStack.push(s);
            if (InfixToPostfix.isOperation(s)){
               double a;
               double b;
               a = Double.parseDouble(workStack.pop());
               b = Double.parseDouble(workStack.pop());
               switch (s){
                   case "+":
                       workStack.push(String.valueOf(a + b));
                       break;
                   case "*":
                       workStack.push(String.valueOf(a * b));
                       break;
                   case "-":
                       workStack.push(String.valueOf(b - a));
                       break;
                   case "/":
                       workStack.push(String.valueOf(b / a));
                       break;
                   case "^":
                       workStack.push(String.valueOf(Math.pow(b, a)));
                       break;
               }

            }
        }
        return Double.parseDouble(workStack.pop());
    }
    public static String[] clearNull(String[] array){
        List<String> values = new ArrayList<String>();
        for (String data: array) {
            if (data != null) {
                values.add(data);
            }
        }
        return values
                .toArray(new String[values.size()]);
    }
}
