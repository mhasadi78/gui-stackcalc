package sample;

public class Stack {
    public int top = -1;
    private String[] stack;

    public Stack(){
        stack = new String[500];
    }

    void push(String item){
        if(this.top > 499){
            System.out.println("Error : Stack Overflow");
        }
        else {
            this.top++;
            this.stack[this.top] = item;
        }
    }

    public boolean isEmpty(){
        return this.top == -1;
    }

    String pop(){
        if (this.isEmpty()){
            System.out.println("Error : Stack Underflow");
            return "";
        }
        else {
            this.top--;
            return this.stack[this.top + 1];
        }
    }

    public String peek () {
        if (this.isEmpty()){
            System.out.println("Stack Underflow");
            return "";
        }
        else {
            return this.stack[this.top];
        }
    }

    public Stack reverse() {
        Stack tempStack = new Stack();
        String tempVariable;
        while (!this.isEmpty()){
            tempVariable = this.pop();
            tempStack.push(tempVariable);
        }
        return tempStack;
    }
}