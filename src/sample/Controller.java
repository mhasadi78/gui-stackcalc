package sample;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    List<String> inExp = new ArrayList<>();
    private String temp = "";

    @FXML
    private TextField postExpField;
    @FXML
    private TextField numberField;


    @FXML
    void numberButton(Event evtNB){
        Label lb = (Label) evtNB.getSource();
        String previousText = postExpField.getText();
        postExpField.setDisable(true);
        postExpField.setStyle(
                "-fx-text-fill: white;" +
                " -fx-background-color:  #3b3b4f;" +
                " -fx-opacity: 1;" +
                " -fx-border-color: white"
        );
        temp += lb.getText();
        System.out.println("Temp at this moment " + temp);
        postExpField.setText(previousText + lb.getText());
    }
    @FXML
    void operatorButton(Event evtOP){
        if (!temp.equals("")){
            inExp.add(temp);
            temp = "";
        }
        Label lb = (Label) evtOP.getSource();
        inExp.add(lb.getText());
        System.out.println("inExp at this moment " + inExp);
        String previousText = postExpField.getText();
        postExpField.setDisable(true);
        postExpField.setStyle(
                "-fx-text-fill: white;" +
                " -fx-background-color:  #3b3b4f;" +
                " -fx-opacity: 1;" +
                " -fx-border-color: white"
        );
        postExpField.setText(previousText + lb.getText());

    }
    @FXML
    void changeBgColorEntering(Event evtBgEn){
        Label lb = (Label) evtBgEn.getSource();
        lb.setStyle("-fx-background-color: #a7a1c9");
    }
    @FXML
    void changeBgColorExiting(Event evtBgEx){
        Label lb = (Label) evtBgEx.getSource();
        lb.setStyle("-fx-background-color: #3b3b4f");

    }
    @FXML
    void delWholeString(){
        postExpField.setText("");
        numberField.setText("");
        inExp.clear();
        temp = "";
    }

    @FXML
    void left_par(Event lParEvt){
        Label lb = (Label) lParEvt.getSource();
        String previousText = postExpField.getText();
        inExp.add(lb.getText());
        System.out.println("inExp at this moment " + inExp);
        postExpField.setText(previousText + lb.getText());

    }
    @FXML
    void right_par(Event rParEvt){

        Label lb = (Label) rParEvt.getSource();
        String previousText = postExpField.getText();
        inExp.add(temp);
        temp = "";
        inExp.add(lb.getText());
        postExpField.setText(previousText + lb.getText());
        System.out.println("inExp at this moment " + inExp);

    }
    @FXML
    void equalButton(){
        if (!temp.equals("")){
            inExp.add(temp);
            temp = "";
            System.out.println("inExp at this moment " + inExp);
        }
        String[] inExpStr, postExpStr, nullFreeExp;
        inExpStr = inExp.toArray(new String[0]);
        System.out.println("inExp at this moment " + inExp);
        double result;
        postExpStr = InfixToPostfix.toPostfix(inExpStr);
        System.out.println(Arrays.toString(postExpStr));
        nullFreeExp = Evaluate.clearNull(postExpStr);
        result = Evaluate.evaluate(nullFreeExp);
        System.out.println(Arrays.toString(nullFreeExp));
        int rp = 0, lp = 0;
        for (String s : inExp){
            switch (s){
                case "(":
                    lp++;
                    break;
                case ")":
                    rp++;
                    break;
            }
        }
        if (rp != lp){
            postExpField.setText("Unbalanced Parenthesis");
            return;
        }
        numberField.setText(String.valueOf(result));
        numberField.setDisable(true);
        numberField.setStyle("-fx-text-fill: white;" +
                " -fx-background-color:  #3b3b4f;" +
                " -fx-opacity: 1;" +
                " -fx-border-color: white");
        postExpField.setStyle(
                "-fx-text-fill: white;" +
                " -fx-background-color:  #3b3b4f;" +
                " -fx-opacity: 1;" +
                " -fx-border-color: white;"
        );
        String joined;
        joined = String.join(" ", nullFreeExp);
        postExpField.setText(joined);
        System.out.println(result);

    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
