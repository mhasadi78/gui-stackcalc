package sample;

import javafx.event.Event;

import java.util.Arrays;
public class testing {
    public static void main(String[] args) {
        double finale;
        String[] my_exp = {
                "(",
                "42",
                "/",
                "3",
                ")",
                "*",
                "2"
//                "/",
//                "3"
        };
        my_exp = InfixToPostfix.toPostfix(my_exp);
        finale = Evaluate.evaluate(my_exp);
        System.out.println(Arrays.toString(my_exp));
        my_exp = Evaluate.clearNull(my_exp);
        System.out.println(Arrays.toString(my_exp));
        System.out.println(finale);
    }
}
