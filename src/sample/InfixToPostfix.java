package sample;

public class InfixToPostfix {
    public static boolean par_check = true;
    public static int rightPar = 0;
    public static int leftPar = 0;
    public static int prec(String op){
        if (op.equals("^") || op.equals("√"))
            return 2;
        else if (op.equals("*") || op.equals("/"))
            return 1;
        else
            return 0;
    }

    public static boolean isNumeric(String numSuspect){
        try {
            Double.parseDouble(numSuspect);
            return true;
        } catch (NumberFormatException e){
            return false;
        }
    }

    public static boolean isOperation (String opSuspect){
        boolean isOperator;
        isOperator = false;
        switch (opSuspect){
            case "*":
            case "+":
            case "-":
            case "/":
            case "^":
            case "√":
                isOperator = true;
                break;
        }
        return isOperator;
    }

    public static String[] toPostfix (String[] infixExp) {
        int p = 0;
        Stack workStack = new Stack();
        String[] postExp = new String[infixExp.length];
        for (String s : infixExp) {
            // if being an operand
            if (isNumeric(s)) {
                postExp[p] = s;
                p++;
            }
            // If (a left parenthesis is found)
            if (s.equals("(")) {
                leftPar++;
                //Push it onto the stack
                workStack.push(s);
            }
            // If (a right parenthesis is found)
            if (s.equals(")")) {
                rightPar++;
                // if the stack is not empty
                // AND the top item is not a left parenthesis
                while (!workStack.isEmpty() &&
                        !workStack.peek().equals("(")) {
                    // Pop the stack and add the popped value to P
                    postExp[p] = workStack.pop();
                    p++;
                }
                // Pop the left parenthesis from the stack and discard it
                workStack.pop();
            }
            // If (an operator is found)
            if (isOperation(s)) {
                // if the stack is empty or
                // if the top element is a left parenthesis
                if (workStack.isEmpty() ||
                        workStack.peek().equals("(")) {
                    // Push the operator onto the stack
                    workStack.push(s);
                } else {
                    // the stack is not empty AND the top of the stack
                    // is not a left parenthesis AND precedence of the
                    // operator <= precedence of the top of the stack
//                    String latest_op = null;
                    while (!workStack.isEmpty() &&
                            !workStack.peek().equals("(") &&
                            prec(s) <= prec(workStack.peek())) {
                        // Pop the stack
//                        latest_op = workStack.pop();
                        // and add the top value to P
                        postExp[p] = workStack.pop();
                        p++;
                    }
//                    if (latest_op != null)
                    //Push the latest operator onto the stack
                        workStack.push(s);
                }
            }
        }
        // While (the stack is not empty)
        while (!workStack.isEmpty()){
            // Pop the stack and add the popped value to Pv
            postExp[p] = workStack.pop();
            p++;
        }
        if (workStack.peek().equals("(") || rightPar != leftPar){
            System.out.println("Error : Unbalanced Parenthesis");
            par_check = false;
        }
        System.out.println("----");
        return postExp;

    }
}
